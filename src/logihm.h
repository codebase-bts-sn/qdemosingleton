#ifndef LOGIHM_H
#define LOGIHM_H

#include <QDialog>

namespace Ui {
class LogIHM;
}

class LogIHM : public QDialog
{
    Q_OBJECT

public:
    static LogIHM * getInstance();
    void trace(QString msg, const QColor & c);
    void close();

 private:
    explicit LogIHM(QWidget *parent = nullptr); // constructeur privé
    ~LogIHM(); // destructeur privé

    Ui::LogIHM *ui;

    // Rendre inaccessibles les constructeurs de copie/déplacement et opérateurs d'affectation/déplacement
    LogIHM(LogIHM const&) = delete;
    LogIHM(LogIHM&&) = delete;
    LogIHM& operator=(LogIHM const&) = delete;
    LogIHM& operator=(LogIHM &&) = delete;

    static LogIHM * singleton;
    static int nbRefs;
};

#endif // LOGIHM_H
