#include "mainihm.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainIHM w;
    w.show();
    return a.exec();
}
