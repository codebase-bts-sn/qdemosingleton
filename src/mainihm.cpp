#include "mainihm.h"
#include "ui_mainihm.h"

MainIHM::MainIHM(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainIHM)
{
    ui->setupUi(this);

    QObject::connect(
      ui->chkbxPB
      , SIGNAL(stateChanged(int))
      , this
      , SLOT(onChkbxPBStateChanged(int))
    );

    QObject::connect(
      ui->chkbxSW
      , SIGNAL(stateChanged(int))
      , this
      , SLOT(onChkbxSWStateChanged(int))
    );

    QObject::connect(
      ui->PB
      , SIGNAL(pressed())
      , this
      , SLOT(onPBPressedOrReleased())
    );

    QObject::connect(
      ui->PB
      , SIGNAL(released())
      , this
      , SLOT(onPBPressedOrReleased())
    );

    QObject::connect(
      ui->SW
      , SIGNAL(clicked())
      , this
      , SLOT(onSWClicked())
    );

    isPBTraced = false;
    isSWTraced = false;
}

MainIHM::~MainIHM()
{
    delete ui;
}

void MainIHM::onChkbxPBStateChanged(int state) {
   if(state == Qt::Checked) {
      isPBTraced = true;
      pLogDlg = LogIHM::getInstance();
   } else {
      isPBTraced = false;
      pLogDlg->close();
   }
}

void MainIHM::onChkbxSWStateChanged(int state) {
   if(state == Qt::Checked) {
      isSWTraced = true;
      pLogDlg = LogIHM::getInstance();
   } else {
      isSWTraced = false;
      pLogDlg->close();
   }
}

void MainIHM::onPBPressedOrReleased() {
   QString state = ui->PB->isDown() ? "ENFONCE" : "RELACHE";

   if(ui->PB->isDown()) {
       ui->PB->setIcon(QIcon(":/img/pressed.svg"));
   } else {
       ui->PB->setIcon(QIcon(":/img/normal.svg"));
   }

   if(isPBTraced) {
      pLogDlg->trace("Bouton poussoir " + state, Qt::darkMagenta);
   }
}

void MainIHM::onSWClicked() {
   QString state = ui->SW->isChecked() ? "ON" : "OFF";

   if(isSWTraced) {
      pLogDlg->trace(
         QString::fromUtf8("Interrupteur à Bascule ") + state
         , Qt::darkGreen
         );
   }
}

