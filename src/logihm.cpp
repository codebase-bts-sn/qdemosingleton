#include <QTime>

#include "logihm.h"
#include "ui_logihm.h"


// Initialisation des attributs static
LogIHM * LogIHM::singleton = nullptr;
int LogIHM::nbRefs = 0;

LogIHM::LogIHM(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LogIHM)
{
    ui->setupUi(this);

    show();
    move(0,0);
}

LogIHM::~LogIHM()
{
    delete ui;
}

/**
 * Méthode qui permet de récupérer l'unique instance de la classe
 */
LogIHM * LogIHM::getInstance() {
    if (singleton == nullptr) {
       singleton = new LogIHM();
    }

   nbRefs++;

   return singleton;
}

/**
 * Détruit l'unique instance lorsqu'elle n'est plus référencée par
 * qui que ce soit.
 */
void LogIHM::close() {
   nbRefs--;
   if(nbRefs == 0) {
      delete singleton;
      singleton = nullptr;
   }
}

/**
 * Trace un évènement
 */
void LogIHM::trace(QString msg, const QColor & color) {
   QString timeStamp = QTime::currentTime().toString("hh:mm:ss.zzz");

   ui->tedtLog->setFontWeight( QFont::DemiBold );
   ui->tedtLog->setTextColor( color );
   ui->tedtLog->append(timeStamp + " -> " + msg + "\n");
}
