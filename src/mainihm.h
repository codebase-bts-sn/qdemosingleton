#ifndef MAINIHM_H
#define MAINIHM_H

#include <QDialog>

#include "logihm.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainIHM; }
QT_END_NAMESPACE

class MainIHM : public QDialog
{
    Q_OBJECT

public:
    MainIHM(QWidget *parent = nullptr);
    ~MainIHM();

private:
    Ui::MainIHM *ui;

    LogIHM * pLogDlg;

    bool isPBTraced;
    bool isSWTraced;

private slots :
    void onChkbxPBStateChanged(int);
    void onChkbxSWStateChanged(int);
    void onPBPressedOrReleased();
    void onSWClicked();

};
#endif // MAINIHM_H
