
# QDemoSingleton

Exemple d'application *Qt* illustrant le *design pattern*  ***Singleton***.

L'application permet de tenir un fichier journal (→ log) dans lequel les actions sur le bouton poussoir et l'interrupteur de l'IHM seront — de manière optionnelle et à la demande — reportées.

![alt text](img/demo-singleton.png  "Qt App screenshot")

L'utilisation du *design pattern* ***Singleton*** permet de s'assurer que :

1. un seul journal sera créé/utilisé quel que soit le composant d’où émane la 1ière demande

2. le fichier journal sera fermé lorsque le dernier des composants à l’utiliser mettra fin à la journalisation

![alt text](img/singleton-solution.svg  "Design pattern Singleton")



